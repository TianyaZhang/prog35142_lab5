﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ZHANG_TLAB5;
using ZHANG_TLAB5.Models;


namespace ZHANG_TLAB5.ViewModels
{
    public class UsersViewModel
    {
        public User User { get; set; }
        public IEnumerable<SelectListItem> AllGames { get; set; } //all existing games


        private List<int> _gamesWon;
        private List<int> _gamesLost;
        public List<int> GamesWon
        {
            get
            {
                if (_gamesWon == null)
                {
                    _gamesWon = User.GamesWon.Select(m => m.GameId).ToList();
                }
                return _gamesWon;
            }
            set { _gamesWon = value; }
        }

        public List<int> GamesLost
        {
            get
            {
                if (_gamesLost == null)
                {
                    _gamesLost = User.GamesLost.Select(m => m.GameId).ToList();
                }
                return _gamesLost;
            }
            set { _gamesLost = value; }
        }

    }

    public static class ViewModelHelpers
    {
        public static UsersViewModel ToViewModel(this User user)
        {
            var model = new UsersViewModel
            {
                User = user
            };

            return model;
        }

        public static User ToDomainModel(this UsersViewModel model)
        {
            var user = new User();
            user.UserId = user.UserId;
            user.UserName = user.UserName;
            user.Email = user.Email;

            return user;
        }
    }
}