﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ZHANG_TLAB5.Models
{
    public class ZHANG_TLAB5Context : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public ZHANG_TLAB5Context() : base("name=ZHANG_TLAB5Context")
        {
        }

        public System.Data.Entity.DbSet<ZHANG_TLAB5.Game> Games { get; set; }

        public System.Data.Entity.DbSet<ZHANG_TLAB5.User> Users { get; set; }
    }
}
